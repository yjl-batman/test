const helloWorld = {
  methods: {
    handleClose(done) {
      done();
    },
    onSubmit() {
      let param = { username: this.login.name, email: this.login.name, passwd: this.login.passwd }
      let url = '/login/checkUser'
      this.axios.post(url, param).then((res) => {
        if (res.data == 200) {
          this.$message('系统未上线，敬请期待')
        } else if(res.data == 500){
          this.$message('用户不存在')
          this.lightStar()
        }else if(res.data == 400){
          this.$message('用户或密码错误')
        }
      }, (error) => {
        this.$message(error)
        console.log(error)
      })
    },
    addSubmit() {
      //邮箱校验
      if (!(/^([a-zA-Z]|[0-9])(\w|\-)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,4})$/.test(this.addlogin.email))) {
        this.$message("邮箱格式错误")
        return
      }
      if(!this.addlogin.email|| !this.addlogin.name || !this.addlogin.passwd){
        this.$message("全为必填项")
        return
      }
      let param = { email: this.addlogin.email,username:this.addlogin.name,passwd:this.addlogin.passwd }
      let url = '/login/checkUser'
      this.axios.post(url, param).then((res) => {
        if (res.data == 900) {
          this.$message('用户存在')
        } else if(res.data == 400) {
          this.$message('用户存在')
        }else if(res.data ==500){
          this.sendEmail()
        }
      }, (error) => {
       this.$message(error)
       console.log(error)
      })
    },
    sendEmail() {
      this.loading = true;
      this.addInfo = "稍等";
      if (this.needSend) {
        let param = { email: this.addlogin.email, username: this.addlogin.name, passwd: this.addlogin.passwd, asscode: this.addlogin.asscode }
        let url = '/login/addUser'
        this.axios.post(url, param).then((res) => {
          if (res.data) {
            this.$message('注册成功')
            this.addUserShow = false
          } else {
            this.$message('注册失败，请检查输入')
            this.loading = false;
            this.addInfo = "提交";
          }
        }, (error) => {
          this.$message(error)
          console.log(error)
        })
        return
      }
      let param = { params: { to: this.addlogin.email } }
      let url = '/email/send'
      this.axios.get(url, param).then((res) => {
        if (res.data === 200) {
          this.loading = false;
          this.addInfo = "发送成功";
          this.disButton = true;
          this.otherDisButton = true
          this.needSend = true
        } else {
          if (res.data > 1000) {
            let time = (res.data + 300000 - new Date().getTime())
            let m = new Date(time).getMinutes() === 0 ? '' : new Date(time).getMinutes() + '分'
            let s = new Date(time).getSeconds() === 0 ? '' : new Date(time).getSeconds() + '秒'
            this.$message('请' + m + '' + s + "后再试")
            this.loading = false;
            this.addInfo = "发送验证码";
          } else if (res.data === 500) {
            this.$message("系统错误")
            this.disButton = true
          } else {
            this.$message("未收录错误")
            this.disButton = true
          }
        }
      }, (error) => {
        this.$message(error)
        console.log(error)
      })
    },
    lightStar() {
      let num = 0
      let addOrdel = true
      let star = setInterval(() => {
        if (num === 10) {
          addOrdel = false
        }
        if (num === 0) {
          addOrdel = true
        }
        if (addOrdel) {
          num++
        } else {
          num--
        }
        this.showLight = { 'box-shadow': 'red 0px 0px 10px ' + num + 'px' }
      }, 50);
      setTimeout(() => {
        clearInterval(star)
        this.showLight = {}
      }, 5000);
    },
    changeImg(e) {
      this.background = { background: "url(" + this.imgObjSave[parseInt(e.currentTarget.name)].imgSrc + ")" };
    },
    titleImg(e) {
      if (this.canTitle) {
        this.$notify({
          title: '提示',
          message: '可以双击',
          position: 'top-left'
        });
      }
      this.canTitle = false
    }
  },
}
export default helloWorld